package net.ukr.wumf;

public class Main {

	public static void main(String[] args) {

		Network vodafone = new Network("Vodafone");
		Phone ph1 = new Phone(380663152525L);
		ph1.setRegistrationInNetwork(vodafone);

		Phone ph2 = new Phone(380507651010L);
		ph2.setRegistrationInNetwork(vodafone);

		Phone ph3 = new Phone(380668907878L);
		ph3.setRegistrationInNetwork(vodafone);

		Phone ph4 = new Phone(380504457735L);
		ph4.setRegistrationInNetwork(vodafone);

		System.out.println(vodafone.toString());

		ph2.call(380668907878L);
	}

}
